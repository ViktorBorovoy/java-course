package org.example.generics;

public class ObjectProcessor<T extends User> {

  private T object;

  public <E> E getMetadata(E input) {
    return input;
  }

  public String getMetadata(String input) {
    return input;
  }
}
