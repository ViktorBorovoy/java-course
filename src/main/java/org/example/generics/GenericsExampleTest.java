package org.example.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsExampleTest {
  public static void main(String[] args) {
    List<String> strings = new ArrayList<>();
    strings.add("sdsd");
//    strings.add(12);
//    strings.add(new Boolean(true));

    String s = (String) strings.get(0);

    System.out.println(s);

    ObjectProcessor<User> processor = new ObjectProcessor<>();

    Integer metadata = processor.getMetadata(25);

    Double metadata1 = processor.getMetadata(25.6);

    User metadata2 = processor.getMetadata(new UserImpl());

//    only extends user
//    ObjectProcessor<Integer> processor1 = new ObjectProcessor<>();
  }
}
