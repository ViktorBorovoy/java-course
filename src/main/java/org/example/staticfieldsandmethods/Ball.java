package org.example.staticfieldsandmethods;

public class Ball {

    public static int ballsCount = 0;

    private final double radius;

    public Ball(double radius) {
        this.radius = radius;

        ballsCount++;
    }

    public double getRadius() {
        return radius;
    }
}
