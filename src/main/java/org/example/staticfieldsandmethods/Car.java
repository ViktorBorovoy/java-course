package org.example.staticfieldsandmethods;

 public class Car {
    public static int wheelsCountStatic = 4;

    public int wheelsCount = 4;

    public Car(int wheelsCount) {
        this.wheelsCount = wheelsCount;
    }

    public int getWheelsCount() {
        return wheelsCount;
    }

    public void setWheelsCount(int wheelsCount) {
        this.wheelsCount = wheelsCount;
    }

    public static void setWheelsCountStatic(int wheelsCountStatic) {
        Car.wheelsCountStatic = wheelsCountStatic;
    }

    public int getWheelsCountStatic() {
        return wheelsCountStatic;
    }
}


