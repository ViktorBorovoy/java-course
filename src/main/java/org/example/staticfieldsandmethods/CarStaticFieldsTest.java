package org.example.staticfieldsandmethods;

public class CarStaticFieldsTest {
    public static void main(String[] args) {
        Car car = new Car(4);
        Car car2 = new Car(2);

        Car car3 = new Car(8);
        car3.setWheelsCount(5);

        System.out.println(car.getWheelsCount());
        System.out.println(car2.getWheelsCount());
        System.out.println(car3.getWheelsCount());


        Car car1 = new Car(4);
        Car car12 = new Car(2);
        Car car13 = new Car(8);

        car12.setWheelsCountStatic(5);

        System.out.println(car1.getWheelsCountStatic());
        System.out.println(car12.getWheelsCountStatic());
        System.out.println(car13.getWheelsCountStatic());
    }
}
