package org.example.io;

import java.io.*;

public class ReadFileExample {
  public static void main(String[] args) {
    File file = new File("/E:/Project/java-course/test.txt");
    try (FileInputStream fileInputStream = new FileInputStream(file)) {
      int b;
      while ((b = fileInputStream.read()) != -1) {
        System.out.println((char) b);
      }
    } catch (IOException e) {
      System.out.println(e);
    }

    try (BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(file), 2048)) {
      int b;
      while ((b = fileInputStream.read()) != -1) {
        System.out.println((char) b);
      }
    } catch (IOException e) {
      System.out.println(e);
    }

    try (BufferedReader fileInputStream = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = fileInputStream.readLine()) != null) {
        System.out.println(line);
      }
    } catch (IOException e) {
      System.out.println(e);
    }

//    FileInputStream fileInputStream = null;
//    try {
//      fileInputStream = new FileInputStream("test.txt")
//    } catch (FileNotFoundException e) {
//      System.out.println(e);
//    } finally {
//      if (fileInputStream != null) {
//        try {
//          fileInputStream.close();
//        } catch (IOException e) {
//          System.out.println(e);
//        }
//      }
//    }
  }
}
