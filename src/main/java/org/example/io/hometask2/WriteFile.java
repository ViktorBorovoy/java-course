package org.example.io.hometask2;

import org.example.io.hometask.FileException;

import java.io.*;
import java.util.List;

public class WriteFile {
    public File write(List<String> newList, String pathname) throws IOException {

        File file = new File(pathname);

        try {
            if (!file.exists()) {
                file.createNewFile();
                FileWriter writer = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(writer);

                for (String s : newList) {
                    bufferedWriter.write(s);
                    bufferedWriter.newLine();
                }

                bufferedWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
