package org.example.io.hometask2;

import org.example.io.hometask.FileException;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {

    public List<String> read(String pathname) {
        File file = new File(pathname);

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String line;
            List<String> stringList = new ArrayList<>();
            while ((line = in.readLine()) != null) {
                stringList.add(line);
            }
            return stringList;
        } catch (IOException e) {
            RuntimeException runTimeException = new RuntimeException(e);
            throw runTimeException;
        }
    }
}
