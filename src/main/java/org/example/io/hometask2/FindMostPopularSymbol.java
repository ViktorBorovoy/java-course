package org.example.io.hometask2;

import java.util.*;

public class FindMostPopularSymbol {
    private final List<String> marks;

    public FindMostPopularSymbol() {
        this.marks = new ArrayList<>();
        marks.add(",");
        marks.add(".");
        marks.add("?");
        marks.add("!");
        marks.add("-");
    }

    public SymbolCount findSymbol(List<String> list) {
        Map<Character, Integer> symbolsCount = new HashMap<>();
        for (String textLine : list) {
            char[] symbols = textLine.toCharArray();
            for (char symbol : symbols) {
                Integer symbolCount = symbolsCount.get(symbol);
                if (symbolCount == null && !marks.contains(symbol)) {
                    symbolsCount.put(symbol, 1);
                } else if (symbolCount != null) {
                    symbolsCount.put(symbol, ++symbolCount);
                }
            }
        }
        int maxValueInMap = (Collections.max(symbolsCount.values()));
        Set<Map.Entry<Character, Integer>> entries = symbolsCount.entrySet();
        for (Map.Entry<Character, Integer> entry : entries) {
            if (entry.getValue() == maxValueInMap) {

                return new SymbolCount(entry.getKey().toString(), entry.getValue());
            }
        }
        throw new RuntimeException("Symbol is not found");
    }
}
