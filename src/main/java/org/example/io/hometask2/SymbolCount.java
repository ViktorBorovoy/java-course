package org.example.io.hometask2;

public class SymbolCount {
    private final String symbol;
    private final int count;

    public SymbolCount(String symbol, int count) {
        this.symbol = symbol;
        this.count = count;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getCount() {
        return count;
    }
}
