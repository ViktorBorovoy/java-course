package org.example.io.hometask2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FindCountSymbolManager {
    private String firstPathname;
    private String secondPathname;
    private String thirdPathname;

    public FindCountSymbolManager(String firstPathname, String secondPathname, String thirdPathname) {
        this.firstPathname = firstPathname;
        this.secondPathname = secondPathname;
        this.thirdPathname = thirdPathname;
    }
    public void calculate() throws IOException {
        ReadFile readFile = new ReadFile();
        WriteFile writeFile = new WriteFile();
        FindMostPopularSymbol findMostPopularSymbol = new FindMostPopularSymbol();
        List<String> list1 = readFile.read(firstPathname);

        writeFile.write(list1, secondPathname);
        List<String> list2 = readFile.read(secondPathname);

        SymbolCount symbolCount = findMostPopularSymbol.findSymbol(list2);

        int count = symbolCount.getCount();
        String symbol = symbolCount.getSymbol();

        String popularSymbol = "Most popular symbol is:" + symbol;
        String popularSymbolCount = "Count of most popular symbol is:" + count;

        List<String> list3 = new ArrayList<>();
        list3.add(popularSymbol);
        list3.add(popularSymbolCount);

        writeFile.write(list3, thirdPathname);
    }
}
