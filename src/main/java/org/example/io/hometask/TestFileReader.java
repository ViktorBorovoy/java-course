package org.example.io.hometask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestFileReader {
    public List<String> readText(String pathname) throws FileException {
        File fileText = new File(pathname);

        try (BufferedReader textReader = new BufferedReader(new FileReader(fileText))) {
            String line;
            List<String> stringList = new ArrayList<>();
            while ((line = textReader.readLine()) != null) {
                stringList.add(line);
            }
            return stringList;
        } catch (IOException e) {
            throw new FileException(e.getMessage());
        }
    }
}