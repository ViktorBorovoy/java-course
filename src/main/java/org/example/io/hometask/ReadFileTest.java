package org.example.io.hometask;
import java.util.List;

public class ReadFileTest {
    public static void main(String[] args) {
        OddCountDefinder oddCountDefinder = new OddCountDefinder();
        SymbolCounter symbolCounter = new SymbolCounter();
        TestFileReader testFileReader = new TestFileReader();

        try {
            List<String> testRead = testFileReader.readText("E:/Project/java-course/test.txt");
            int symbolCount = symbolCounter.count(testRead, 'd');
            System.out.println(symbolCount);
            oddCountDefinder.define(symbolCount);
        } catch (FileException e) {
            System.out.println(e.getMessage());
        }
    }
}
