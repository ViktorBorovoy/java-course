package org.example.io.hometask;

public class FileException extends Exception {
    public FileException(String message) { super(message); }
}
