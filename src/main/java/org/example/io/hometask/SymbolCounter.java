package org.example.io.hometask;
import java.util.List;

public class SymbolCounter {
    public int count (List<String> stringList, char symbol) {
        int count = 0;
        for (int index = 0; index < stringList.size(); index++) {
            String line1 = stringList.get(index);
            char[] symbols = line1.toCharArray();
            for (int i = 0; i < symbols.length; i++) {
                char a = symbols[i];
                if (a == symbol) {
                    count++;
                }
            }
        }
        return count;
    }
}
