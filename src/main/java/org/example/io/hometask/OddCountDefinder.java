package org.example.io.hometask;

public class OddCountDefinder {
    public void define (int count) throws FileException {
        if (count % 2 != 0) {
            throw new FileException ("count of symbols 'd' is odd");
        }
    }
}
