package org.example.io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WriteFileExample {
  public static void main(String[] args) {
    List<String> stringList = new ArrayList<>();
    stringList.add("sdsdsdsdsdsds");
    stringList.add("ddddddddd");
    stringList.add("sdsdsdsdsdsds");
    stringList.add("dddddddddd");


    File file = new File("write_test.txt");
    try {
      boolean isCreated = file.createNewFile();
      if (!isCreated) {
        System.out.println("File can not be created.");
      }
      FileWriter writer = new FileWriter(file);
      BufferedWriter bufferedWriter = new BufferedWriter(writer);

      for (String s : stringList) {
        bufferedWriter.write(s);
        bufferedWriter.newLine();
      }

      bufferedWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

//    append to file example
    try {
      boolean isCreated = file.createNewFile();
      if (!isCreated) {
        System.out.println("File can not be created.");
      }

      FileOutputStream outputStream = new FileOutputStream(file, true);
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

      for (String s : stringList) {
        bufferedOutputStream.write(s.getBytes());
        bufferedOutputStream.write('\n');
      }

      bufferedOutputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
