package org.example.exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionsExample {
  public static void main(String[] args) {
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(new File("test.txt"));
    } catch (FileNotFoundException e) {
      System.out.println(e);

      try {
        fileInputStream.close();
      } catch (IOException ioException) {
        System.out.println(ioException);
      }
    } finally {
      System.out.println("finally block");
    }



//    optional blocks
    try {

    }finally {

    }

    try{

    }catch (Exception e) {

    }

//    checked
//    FileInputStream fileInputStream1 = new FileInputStream("file.txt");


//    unchecked (NullPointerException)
      String x = null;

      x.split(".");


    try {
      readFile("test.txt");
    } catch (IOException e) {
      System.out.println("the error details:" + e);
    }
  }

  public static void readFile(String fileName) throws IOException {
    FileInputStream stream = new FileInputStream(fileName);

    int read = stream.read();

    System.out.println(read);
  }


  private int whatTheResult() {
    try {
      return 1/0;
    } catch (RuntimeException e) {
      return 2;
    }
  }
}
