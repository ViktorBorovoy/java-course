package org.example.exceptions.app;

import java.util.List;

public class UserValidator {

  public void validateUser(User user, List<User> users) throws ValidationException {
    for (User dbUser : users) {
      if (dbUser.getEmail().equals(user.getEmail())) {
        throw new ValidationException(String.format("The users with email %s already exists", user.getEmail()));
      }
    }
  }
}
