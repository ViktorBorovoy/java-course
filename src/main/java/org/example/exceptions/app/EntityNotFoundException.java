package org.example.exceptions.app;

public class EntityNotFoundException extends Exception {
  public EntityNotFoundException(String message) {
    super(message);
  }
}
