package org.example.exceptions.app;

public class AppTest {
  public static void main(String[] args) {
    User user = new User();
    user.setId(26867676767676763L);
    user.setName("Petro");
    user.setEmail("petro@gmail.com");

    User user1 = new User();
    user1.setId(26867676767343433L);
    user1.setName("Anton");
    user1.setEmail("petro@gmail.com");

    UserValidator userValidator = new UserValidator();
    UserService userService = new UserService(userValidator);

    try {
      userService.addUser(user);

      userService.addUser(user1);
    } catch (ValidationException e) {
      System.out.println(e.getMessage());
    }

    try {
      User userById = userService.findUserById(12L);

      System.out.println(userById);
    } catch (EntityNotFoundException e) {
      System.out.println(e.getMessage());
    }

    userService.printUsersDetails();

  }
}
