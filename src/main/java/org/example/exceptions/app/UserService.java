package org.example.exceptions.app;

import java.util.ArrayList;
import java.util.List;

public class UserService {

  private List<User> users = new ArrayList<>();

  private final UserValidator userValidator;

  public UserService(UserValidator userValidator) {
    this.userValidator = userValidator;
  }

  public void addUser(User user) throws ValidationException {
    userValidator.validateUser(user, users);

    users.add(user);
  }

  public User findUserById(Long id) throws EntityNotFoundException {
    for (User user : users) {
      if (user.getId().equals(id)) {
        return user;
      }
    }

    throw new EntityNotFoundException(String.format("The user with id: %d not found", id));
  }

  public void printUsersDetails() {
    for (User user : users) {
      System.out.println(user);
    }
  }
}
