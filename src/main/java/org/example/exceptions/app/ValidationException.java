package org.example.exceptions.app;

public class ValidationException extends Exception {
  public ValidationException(String message) {
    super(message);
  }
}
