package org.example.collections.arraylist;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class LinkedListTest {
  public static void main(String[] args) {
    List<String> list = new LinkedList<>();
    list.add("a");

    Iterator<String> iterator = list.iterator();
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }

    LinkedList<String> llist = new LinkedList<>();

    llist.addFirst("a");
    llist.addLast("b");

    String s = llist.pollFirst();

    String s1 = llist.pollLast();

    Iterator<String> stringIterator = llist.descendingIterator();

    ListIterator<String> stringListIterator = llist.listIterator();
  }
}
