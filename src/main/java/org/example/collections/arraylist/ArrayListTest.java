package org.example.collections.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListTest {
  public static void main(String[] args) {
    int[] intsArray = new int[]{1, 2, 3};

    int[] ints = Arrays.copyOf(intsArray, 10);

    List<String> list = new ArrayList<>();
    list.add("a");
    list.add("b");
    list.add("c");
    list.add("d");
    list.add("d");
    list.add("m");

    list.remove("d");
    list.remove(2);

    List<String> anotherList = new ArrayList<>();
    anotherList.add("a");
    anotherList.add("b");
    anotherList.add("c");
    anotherList.add("d");
    anotherList.add("m");

    list.addAll(anotherList);

    list.add(0, "a");
    list.set(2, "d");

    list.clear();

    boolean contains = list.contains("a");

    boolean containsAll = list.containsAll(anotherList);

    String s = list.get(1);
    int index = list.indexOf("d");

    boolean empty = list.isEmpty();

    int size = list.size();

    Object[] objects = list.toArray();

    List<String> strings = list.subList(3, 3);

    System.out.println(list);


  }
}
