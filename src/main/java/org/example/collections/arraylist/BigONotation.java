package org.example.collections.arraylist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BigONotation {
  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add(0, "a");
//    add O(1)
    list.add("a");
//    delete O(1)
    list.remove(3);

//    delete O(n)
    list.remove("a");
//    search O(n)
    list.contains("a");
//    get O(1)
    list.get(3);

    list.add(250000, "4");


    List<String> llist = new LinkedList<>();

    llist.add(0, "a");
//    add O(1)
    llist.add("a");

//    delete O(n)
    llist.remove(5);
    llist.remove("a");

    //    search O(n)
    llist.contains("a");

//    get O(n)
    llist.get(3);

    list.add(250000, "4");
  }
}
