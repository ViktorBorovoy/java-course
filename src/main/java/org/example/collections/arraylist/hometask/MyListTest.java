package org.example.collections.arraylist.hometask;

import java.util.ArrayList;

public class MyListTest {
    public static void main(String[] args) {
        MyList myList = new MyList(3);

        myList.add(0);
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);
        myList.add(5);
        myList.add(6);
        myList.add(7);
        myList.add(8);
        myList.add(9);

        for (int i = 0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }

        System.out.println(myList.contains(1));

    }
}
