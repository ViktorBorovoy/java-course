package org.example.collections.arraylist.hometask;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyList implements List {
    private Object[] array;
    private int index = 0;

    public MyList(int initialCapacity) {
        this.array = new Object[initialCapacity];
    }

    @Override
    public boolean add(Object o) {
        if (index == array.length) {
            int capacity = array.length * 3 / 2 + 1;
            Object[] newArray = new Object[capacity];
            for (int i = 0; i < array.length; i++) {
                Object element = array[i];
                newArray[i] = element;
            }
            this.array = newArray;
        }
        array[index] = o;
        index++;
        return true;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o)  {
        return indexOf(o) >= 0;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }


    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(int index) {
        if (array[index] == null) {

        }
        return this.array[index];
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < array.length; i++)
                if (array[i]==null)
                    return i;
        } else {
            for (int i = 0; i < array.length; i++)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }


    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


//    private int count = 0;
//    private int[] myArray;
//    private int element;
//    private int i;
//    private int initialCapacity;
//
//    public MyList(int initialCapacity) {
//        this.initialCapacity = initialCapacity;
//    }
//
//    public boolean add(int element) {
//        if (count == myArray.length) {
//            i = myArray.length * 3 / 2 + 1;
//            System.arraycopy(myArray, 0, i, 0, myArray.length);
//            this.element = i;
//        }
//        this.myArray[count++] = element;
//        return true;
//    }
}
