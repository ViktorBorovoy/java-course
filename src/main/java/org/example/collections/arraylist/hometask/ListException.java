package org.example.collections.arraylist.hometask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListException {
    public static void main(String[] args) {
        List<String> eList = new ArrayList<>();

        eList.add("UKR");
        eList.add("USA");
        eList.add("ENG");
        eList.add("FRA");
        eList.add("ITA");
        eList.add("Spain");
        eList.add("Turkey");
        eList.add("Germany");
        eList.add("Canada");
        eList.add("Norway");

        for (String s : eList) {
            if (s.length() > 5) {
                throw new RuntimeException("Number of symbols more than 6");
            }
            System.out.println(s);
        }
    }
}
