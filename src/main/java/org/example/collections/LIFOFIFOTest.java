package org.example.collections;

import java.util.LinkedList;

public class LIFOFIFOTest {
  public static void main(String[] args) {
    LinkedList<String> lifo = new LinkedList<>();

    lifo.add("a");
    lifo.add("b");
    lifo.add("c");
    lifo.add("d");
    lifo.add("e");
//    a b c d e

    System.out.println(lifo.removeLast());
    System.out.println(lifo.removeLast());
    System.out.println(lifo.removeLast());
    System.out.println(lifo.removeLast());
    System.out.println(lifo.removeLast());


    LinkedList<String> fifo = new LinkedList<>();

    fifo.add("a");
    fifo.add("b");
    fifo.add("c");
    fifo.add("d");
    fifo.add("e");
//    a b c d e

    System.out.println(fifo.pop());
    System.out.println(fifo.pop());
    System.out.println(fifo.pop());
    System.out.println(fifo.pop());
    System.out.println(fifo.pop());


  }
}
