package org.example.collections.treeset;

public class User implements Comparable<User>{
  private String name;

  private int age;

  public User(String name, int age) {
    this.name = name;
    this.age = age;
  }

  @Override
  public int compareTo(User o) {
    return name.compareTo(o.name);
  }

  @Override
  public String toString() {
    return "User{" +
            "name='" + name + '\'' +
            ", age=" + age +
            '}';
  }
}
