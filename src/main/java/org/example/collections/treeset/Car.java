package org.example.collections.treeset;

public class Car {
  private String model;
  private Integer age;

  public Car(String model, Integer age) {
    this.model = model;
    this.age = age;
  }

  public String getModel() {
    return model;
  }

  public Integer getAge() {
    return age;
  }

  @Override
  public String toString() {
    return "Car{" +
            "model='" + model + '\'' +
            ", age=" + age +
            '}';
  }
}
