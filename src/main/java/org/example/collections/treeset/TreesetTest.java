package org.example.collections.treeset;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreesetTest {
  public static void main(String[] args) {
    Set<String> set = new TreeSet<>();

    set.add("a");
    set.add("d");
    set.add("c");
    set.add("e");
    set.add("b");

    System.out.println(set);


    Set<User> users = new TreeSet<>();

    users.add(new User("a", 12));
    users.add(new User("c", 12));
    users.add(new User("b", 12));

    for (User user : users) {
      System.out.println(user);
    }

    Set<Car> cars = new TreeSet<>(new Comparator<Car>() {
      @Override
      public int compare(Car o1, Car o2) {
        return o1.getModel().compareTo(o2.getModel());
      }
    });

    cars.add(new Car("a", 12));
    cars.add(new Car("c", 12));
    cars.add(new Car("b", 12));

    for (Car car : cars) {
      System.out.println(car);
    }
  }
}
