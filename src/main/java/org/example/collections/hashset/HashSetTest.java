package org.example.collections.hashset;

import java.util.HashSet;
import java.util.Set;

public class HashSetTest {
  public static void main(String[] args) {
    Set<String> set = new HashSet<>();

    set.add("a");
    set.add("b");
    set.add("c");
    set.add("v");
    set.add("i");
    set.add("n");
    set.add("y");
    set.add("u");

    for (String s : set) {
      System.out.println(s);
    }


    int x = "asascdcd".hashCode();

    System.out.println(x);

    int i = x & (16 - 1);

    System.out.println(i);
  }
}
