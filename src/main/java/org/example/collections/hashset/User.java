package org.example.collections.hashset;

import java.util.Objects;

public class User {
  private final Long id;
  private String email;
  private String phone;
  private String name;
  private int age;

  public User(Long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return email.equals(user.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email);
  }

  public static void main(String[] args) {
    User user = new User(6L);
    user.email = "vasya1@gmail.com";

    System.out.println(user.hashCode());
  }
}
