package org.example.collections.map;

import java.util.*;

public class MapExample {
  public static void main(String[] args) {
    Map<String, Integer> map = new HashMap<>();

    map.put("y", 12);
    map.put("q", 13);
    map.put("w", 51);
    map.put("e", 15);
    map.put("hello", 125);

    Integer abc = map.get("abc");

    int maxValueInMap = (Collections.max(map.values()));
    Set<Map.Entry<String, Integer>> entries = map.entrySet();
    for (Map.Entry<String, Integer> entry : entries) {
      if (entry.getValue() == maxValueInMap) {
        System.out.println("-------" + entry.getKey());
      }
    }

    for (Map.Entry<String, Integer> entry : entries) {
      System.out.println(entry.getKey());
      System.out.println(entry.getValue());
    }

//    keys only
    Set<String> strings = map.keySet();

//    only values
    Collection<Integer> values = map.values();
  }
}
