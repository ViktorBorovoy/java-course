package org.example.objectsinteractions;

public class Wheel {
    private String radius;

    public Wheel(String radius) {
        this.radius = radius;
    }

    public String getRadius() {
        return radius;
    }
}
