package org.example.objectsinteractions;

public class Body {
    private Wheel[] wheels;
    private Door[] doors;

    public Body(Wheel[] wheels, Door[] doors) {
        this.wheels = wheels;
        this.doors = doors;
    }

    public Wheel[] getWheels() {
        return wheels;
    }

    public Door[] getDoors() {
        return doors;
    }
}
