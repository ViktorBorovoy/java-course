package org.example.objectsinteractions;

public class Car {
    private String model;
    private int year;
    private String color;

    private Body body;

    public Car(String model, int year, String color, Body body) {
        this.model = model;
        this.year = year;
        this.color = color;
        this.body = body;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public String getColor() {
        return color;
    }

    public Body getBody() {
        return body;
    }
}
