package org.example.andoroperands;

public class AndOrOperandsExample {
    public static void main(String[] args) {
        if (hasSeats() && arrivedInTime()) {
            System.out.println("------------");
            System.out.println("Go away by bus");
        } else {
            System.out.println("------------");
            System.out.println("Go away by walk");
        }

        if (hasSeats() || arrivedInTime()) {
            System.out.println("------------");
            System.out.println("Go away by bus");
        } else {
            System.out.println("------------");
            System.out.println("Go away by walk");
        }
    }

    public static boolean hasSeats() {
        System.out.println("hasSeats");
        return true;
    }

    public static boolean arrivedInTime() {
        System.out.println("arrivedInTime");
        return true;
    }
}
