package org.example.languageconstructions.hometask4;

public class Vector {
    private final double x;
    private final double y;
    private final double z;

    private Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double calculateLength() {
        double result = Math.sqrt(x * x + y * y + z * z);
        return result;
    }

    public double calculateScalarProduct(Vector vector) {
        double scalar = this.x * vector.x + this.y * vector.y + this.z * vector.z;
        return scalar;
    }

    public static Vector[] generateVectors(int n) {
        Vector[] vectors = new Vector[n];
        for (int i = 0; i < vectors.length; i++) {
            Vector vector = new Vector(Math.random(), Math.random(), Math.random());
            vectors[i] = vector;
        }
        return vectors;
    }
}
