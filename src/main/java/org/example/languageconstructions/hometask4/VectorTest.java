package org.example.languageconstructions.hometask4;

public class VectorTest {
    public static void main(String[] args) {
        Vector[] vectors = Vector.generateVectors(7);

        Vector v1 = vectors[0];
        System.out.println(v1.calculateLength());

        Vector v2 = vectors[1];
        Vector v3 = vectors[2];
        System.out.println(v2.calculateScalarProduct(v3));

    }
}
