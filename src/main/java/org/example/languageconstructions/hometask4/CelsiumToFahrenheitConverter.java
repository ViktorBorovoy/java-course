package org.example.languageconstructions.hometask4;

public class CelsiumToFahrenheitConverter implements BaseConverter {

    @Override
    public double convert(double temperature) {
        double result = (temperature * 9/5) + 32;
        return result;
    }
}
