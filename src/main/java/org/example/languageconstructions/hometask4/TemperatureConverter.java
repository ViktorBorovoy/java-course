package org.example.languageconstructions.hometask4;

public class TemperatureConverter {
    public static void main(String[] args) {
        BaseConverter celsiumToKelvinConverter = new CelsiumToKelvinConverter();
        BaseConverter celsiumToFahrenheitConverter = new CelsiumToFahrenheitConverter();

        double kelvin = celsiumToKelvinConverter.convert(12);
        System.out.println(kelvin);

        double fahrenheit = celsiumToFahrenheitConverter.convert(15);
        System.out.println(fahrenheit);
    }
}

