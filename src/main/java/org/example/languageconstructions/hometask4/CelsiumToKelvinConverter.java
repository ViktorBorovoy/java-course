package org.example.languageconstructions.hometask4;

public class CelsiumToKelvinConverter implements BaseConverter {
    @Override
    public double convert(double temperature) {
        double result = temperature + 273.15;
        return result;
    }
}
