package org.example.languageconstructions.hometask4;


public class HelloWorld {
    public static void main(String[] args) {
        String str = "Hello beautiful world";
        char[] symbols = str.toCharArray();
        String result = "";
        for (int i = symbols.length - 1; i >= 0; i--) {
            String symbol = String.valueOf(symbols[i]);
            if (" ".equals(symbol)) {
                String nextElement = String.valueOf(symbols[i-1]);
                result += " " + nextElement.toUpperCase();
                i--;
            }else {
                result += symbols[i];
            }
        }
        System.out.println(result);
    }
}


