package org.example.languageconstructions.hometask4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestTask2 {
    public static void main(String[] args) {
        List<String> colors = new ArrayList<>();
        List<String> anotherColors = new ArrayList<>();

        colors.add("black");
        colors.add("yellow");
        colors.add("blue");
        colors.add("green");
        colors.add("white");
        colors.add("grey");
        colors.add("yellow");
        colors.add("orange");
        colors.add("green");
        colors.add("white");

        anotherColors.add("yellow");
        anotherColors.add("purple");
        anotherColors.add("pink");
        anotherColors.add("brown");
        anotherColors.add("green");
        anotherColors.add("yellow");
        anotherColors.add("red");
        anotherColors.add("orange");
        anotherColors.add("black");
        anotherColors.add("green");

        for (String color : colors) {
            if (anotherColors.contains(color)) {
                System.out.println(color);
            }
        }
        Map<String, Integer> colorsMap = groupList(colors);
        Map<String, Integer> colorsMap1 = groupList(anotherColors);
        Map<String, Integer> resultMap = new HashMap<>();


        for (Map.Entry<String, Integer> entry : colorsMap1.entrySet()) {
            Integer count = colorsMap.get(entry.getKey());
            if (count == null) {
                resultMap.put(entry.getKey(), entry.getValue());
            } else {
                resultMap.put(entry.getKey(), entry.getValue() + count);
            }
        }
        for (Map.Entry<String, Integer> entrys : colorsMap.entrySet()) {
            Integer count = colorsMap1.get(entrys.getKey());
            if (count == null) {
                resultMap.put(entrys.getKey(), entrys.getValue());
            } else {
                resultMap.put(entrys.getKey(), entrys.getValue() + count);
            }
        }

        System.out.println(colorsMap);
        System.out.println(colorsMap1);
        System.out.println(resultMap);
    }

    public static Map<String, Integer> groupList(List<String> list) {
        Map<String, Integer> colorMap = new HashMap<>();
        for (String color : list) {
            Integer colorsCount = colorMap.get(color);
            if (colorsCount == null) {
                colorMap.put(color, 1);
            } else {
                colorMap.put(color, ++colorsCount);
            }
        }
        return colorMap;
    }
}
