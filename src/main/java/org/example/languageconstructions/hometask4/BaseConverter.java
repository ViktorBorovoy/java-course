package org.example.languageconstructions.hometask4;

public interface BaseConverter {

    double convert(double temperature);

}
