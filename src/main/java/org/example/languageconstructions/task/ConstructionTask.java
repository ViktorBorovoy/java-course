package org.example.languageconstructions.task;

import java.util.ArrayList;
import java.util.List;

public class ConstructionTask {
    //1
    private String anotherWord = "hi";

    public void ifelseExercise() {
        //2
        int x = 1;
        List<String> myList = new ArrayList<>();
        for (String string : myList) {
            //3
            String word = "Hello";
            if (string.isEmpty()) {
                //4
                String str = "Abc";

            } else if (string.startsWith("a")) {
                //5
                String str2 = "Cba";
                if (string.endsWith("b")) {
                    //6


                } else {
                    //7


                }

            } else {
                //8


            }
        }
    }

    public int switchExercise(String string) {
        switch (string) {
            case "one":
                return 1;
            case "two":
                return 2;
            case "three":
                return 3;
            default:
                System.out.println("Not Found!!!");
        }
        return -1;
    }

    public void whileExercise() {
        int x = 0;
        while (x >= 15) {
            x++;
        }
    }

    public void doWhileExercise() {
        int x = 0;
        do {
            x++;
        } while (x >= 15);
    }
}
