package org.example.languageconstructions.hometask2;

public class DiscriminantCalculation {
    public static void main(String[] args) {
        int[] inputValues = new int[]{4, 8, 2};
        int a = inputValues[0];
        int b = inputValues[1];
        int c = inputValues[2];

        double d = Math.pow(b, 2) - 4 * a * c;
        double x = -b + Math.sqrt(d) / 2 * a;
        double y = -b - Math.sqrt(d) / 2 * a;

        if (d < 0)
            System.out.println("No roots");

        else if (d == 0)
            System.out.println("root1 = " + x);

        else {
            System.out.println("root1 = " + x);
            System.out.println("root2 = " + y);
        }
    }
}
