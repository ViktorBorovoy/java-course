package org.example.languageconstructions;

public class LanguageConstructionsExample {
    public static void main(String[] args) {
//        if statement
        int floor = 0;
        int elevatorFloor = 1;


        if (floor < elevatorFloor) {
            System.out.println("Moving down");
        } else if (floor > elevatorFloor) {
            System.out.println("Moving up");
        } else {
            System.out.println("Evrika priehal");
        }

//        while/do while statement

        int x = 11;

        while (x > 0) {
            System.out.println(x);
            x--;
        }
        System.out.println("Finish!!");

        int y = 12;
        do {
            System.out.println("do smt...");
            y--;
        } while (y > 10);

//        switch statement

        String name = "Vasek";


        switch (name) {
            case "Vasek":
                System.out.println(name);
                break;
            case "Petro":
                System.out.println(name);
                break;
            case "Igor":
                System.out.println(name);
                break;
            default:
                System.out.println("Not Found!!!");
        }

//         loop(цикл) for
        int[] ar = new int[10];
        ar[3] = 145;
        ar[7] = 100;

        for (int index = 0; index < ar.length; index++) {
            int value = ar[index];
            System.out.println("Index: " + index + " value: " + value);
        }

        System.out.println("While loop");
        int index = 0;
        while (index < ar.length) {
            int value = ar[index];
            System.out.println("Index: " + index + " value: " + value);
            index++;
        }

//        operators:

        int a = 7;
        double b = 6;

//        +
        System.out.println(a + b);
//        -
        System.out.println(a - b);
//        *
        System.out.println(a * b);
//        /
        double result = a / b;
        System.out.println(result);
//        ++ --
        System.out.println(a++);
        System.out.println(--a);
//        %
        System.out.println(a % b);
    }
}

