package org.example.languageconstructions.hometask3;

public class Speakers {
    private String totalWatt;

    public Speakers (String totalWatt) { this.totalWatt = totalWatt; }

    public String getTotalWatt() {
        return totalWatt;
    }
}
