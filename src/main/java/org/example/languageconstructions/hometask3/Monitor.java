package org.example.languageconstructions.hometask3;

public class Monitor {
    private String model;
    private int size;

    public Monitor(String model, int size) {
        this.model = model;
        this.size = size;
    }

    public String getModel() {
        return model;
    }

    public int getSize() {
        return size;
    }

}
