package org.example.languageconstructions.hometask3;

public class PersonalComputerTest {
    public static void main(String[] args) {

        Speakers speakerLeft = new Speakers("88 dB");
        Speakers speakerRight = new Speakers("88 dB");

        Speakers[] speakers = new Speakers[]{speakerLeft, speakerRight};

        SystemUnit systemUnit = new SystemUnit("Asus", "logitech",
                            "Kingston", 4, "Intel");

        Monitor monitor = new Monitor("Samsung", 19);
        Mouse mouse = new Mouse("Trust", "black");
        Keyboard keyboard = new Keyboard("Logitech", "black");



        PersonalComputer personalComputer = new PersonalComputer("Grey", 2017,
                "Windows 10", systemUnit, monitor, mouse, keyboard,speakers);

        String color = personalComputer.getColor();
        int year = personalComputer.getYear();
        String operationSystem = personalComputer.getOperationSystem();

        System.out.println(personalComputer);






    }
}
