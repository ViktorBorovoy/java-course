package org.example.languageconstructions.hometask3;

import java.util.Arrays;

public class PersonalComputer {
    private String color;
    private int year;
    private String operationSystem;

    private SystemUnit systemUnit;
    private Monitor monitor;
    private Mouse mouse;
    private Keyboard keyboard;
    private Speakers[] speakers;

    public PersonalComputer(String color, int year, String operationSystem, SystemUnit systemUnit,
                            Monitor monitor, Mouse mouse, Keyboard keyboard, Speakers[] speakers) {
        this.color = color;
        this.year = year;
        this.operationSystem = operationSystem;
        this.systemUnit = systemUnit;
        this.monitor = monitor;
        this.mouse = mouse;
        this.keyboard = keyboard;
        this.speakers = speakers;
    }

    public String getColor() {
        return color;
    }

    public int getYear() {
        return year;
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public SystemUnit getSystemUnit() {
        return systemUnit;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public Mouse getMouse() {
        return mouse;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public Speakers[] getSpeakers() {
        return speakers;
    }

    @Override
    public String toString() {
        return "PersonalComputer{" +
                "color='" + color + '\'' +
                ", year=" + year +
                ", operationSystem='" + operationSystem + '\'' +
                ", systemUnit=" + systemUnit +
                ", monitor=" + monitor +
                ", mouse=" + mouse +
                ", keyboard=" + keyboard +
                ", speakers=" + Arrays.toString(speakers) +
                '}';
    }
}





