package org.example.languageconstructions.hometask3;

public class Mouse {
    private String mouseModel;
    private String mouseColor;

    public Mouse (String mouseModel, String mouseColor) {
        this.mouseModel = mouseModel;
        this.mouseColor = mouseColor;
    }

    public String getMouseModel() {
        return mouseModel;
    }

    public String getMouseColor() {
        return mouseColor;
    }


}
