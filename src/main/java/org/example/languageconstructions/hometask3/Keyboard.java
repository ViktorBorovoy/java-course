package org.example.languageconstructions.hometask3;

public class Keyboard {
    private String model;
    private String color;

    public Keyboard(String model, String color) {
        this.model = model;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }
}


