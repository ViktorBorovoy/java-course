package org.example.languageconstructions.hometask3;

public class SystemUnit {
    private String motherboard;
    private String powerSupply;
    private String hardDiskDrive;
    private int ram;
    private String cpu;

    public SystemUnit(String motherboard, String powerSupply, String hardDiskDrive, int ram, String cpu) {
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
        this.hardDiskDrive = hardDiskDrive;
        this.ram = ram;
        this.cpu = cpu;
    }

    public String getMotherboard() { return motherboard; }

    public String getPowerSupply() { return powerSupply; }

    public String getHardDiskDrive() { return hardDiskDrive; }

    public int getRam() { return ram; }

    public String getCpu() { return cpu; }

    @Override
    public String toString() {
        return "SystemUnit{" +
                "motherboard='" + motherboard + '\'' +
                ", powerSupply='" + powerSupply + '\'' +
                ", hardDiskDrive='" + hardDiskDrive + '\'' +
                ", ram=" + ram +
                ", cpu='" + cpu + '\'' +
                '}';
    }
}


