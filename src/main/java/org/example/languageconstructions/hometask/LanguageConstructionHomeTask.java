package org.example.languageconstructions.hometask;

public class LanguageConstructionHomeTask {
    public static void main(String[] args) {
        int[] digitsArray = new int[10];
        digitsArray[1] = 10;
        digitsArray[3] = 31;
        digitsArray[5] = 52;
        digitsArray[9] = 94;
        digitsArray[6] = 33;

        System.out.println("For loop");
        int result = 0;
        for (int index = 0; index < digitsArray.length; index++) {
            int value = digitsArray[index];
            result = result + value;
        }
        System.out.println("The sum is: " + result);

        System.out.println("While loop");
        int index = 0;
        int whileResult = 0;
        while (index < digitsArray.length) {
            int value = digitsArray[index];
            whileResult = whileResult + value;
            index++;
        }

        System.out.println("While result: " + whileResult);

        System.out.println("even numbers");
        for (int even = 0; even < digitsArray.length; even++) {

            int value = digitsArray[even];
            if (value % 2 == 0 && value != 0)
                System.out.println(value);
        }


        System.out.println("odd numbers");
        for (int odd = 0; odd < digitsArray.length; odd++) {
            int value = digitsArray[odd];
            if (value % 2 != 0) {
                System.out.println(value);
            }
        }
    }
}

