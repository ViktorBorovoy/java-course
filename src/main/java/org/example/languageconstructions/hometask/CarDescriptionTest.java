package org.example.languageconstructions.hometask;

public class CarDescriptionTest {
    public static void main(String[] args) {
        CarDescription car = new CarDescription(4,"Black","Mercedes", "2019",
                "coupe", "diesel",2);

        String model = car.getModel();
        System.out.println("Model: " + model);

        String color = car.getColor();
        System.out.println("Color: " + color);

        int wheelsCount = car.getWheelsCount();
        System.out.println("Wheels count: " + wheelsCount);

        String year = car.getYear();
        System.out.println("Year: " + year);

        String bodyType = car.getBodyType();
        System.out.println("Body type: " + bodyType);

        String fuelType = car.getFuelType();
        System.out.println("Fuel type: " + fuelType);

        int doorsCount = car.getDoorsCount();
        System.out.println("Doors count: " + doorsCount);

        System.out.println(car);
    }


}
