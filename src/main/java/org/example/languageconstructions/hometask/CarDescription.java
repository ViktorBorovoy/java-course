package org.example.languageconstructions.hometask;

public class CarDescription {
    private int wheelsCount;
    private String color;
    private String model;
    private String year;
    private String bodyType;
    private String fuelType;
    private int doorsCount;

    public CarDescription(int wheelsCount, String color, String model, String year,
                          String bodyType, String fuelType, int doorsCount) {
        this.wheelsCount = wheelsCount;
        this.color = color;
        this.model = model;
        this.year = year;
        this.bodyType = bodyType;
        this.fuelType = fuelType;
        this.doorsCount = doorsCount;
    }

    public void startEngine() {
            System.out.println("Engine is starting!");
        }

    public int getWheelsCount() { return this.wheelsCount; }
    public String getColor() { return this.color; }
    public String getModel() { return this.model; }
    public String getYear() { return this.year; }
    public String getBodyType() { return this.bodyType; }
    public String getFuelType() { return this.fuelType; }
    public int getDoorsCount() { return this.doorsCount; }

    public String toString() {
        return "color: " + color + " model: " + model + " year: " + year + " wheels count: " + wheelsCount
                + " body type: " + bodyType + " fuel type: " + fuelType + " doors count: " + doorsCount;
    }


}
