package org.example.languageconstructions.hometask5;

public class UserValidationException extends Exception{
    public UserValidationException(String message){
        super(message);
    }
}
