package org.example.languageconstructions.hometask5;

public class UserTest {
    public static void main(String[] args) throws UserValidationException, UserNotFoundException {
        User user = new User(213,"Petro", "petro@gmail.com");
        User user1 = new User(215, "null", "null@gmail.com");
        UserValidator userValidator = new UserValidator();
        UserService userService = new UserService(userValidator);


        userService.saveUser(user);
        System.out.println(userService.getUserById(213));

        userService.saveUser(user1);
        System.out.println(userService.getUserById(215));
    }
}
