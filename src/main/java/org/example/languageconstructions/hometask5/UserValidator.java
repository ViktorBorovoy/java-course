package org.example.languageconstructions.hometask5;

public class UserValidator {
    public void validate(User user) throws UserValidationException {
        validate(user.getId(), "Id is null");
        validate(user.getMail(), "mail is null");
        validate(user.getName(), "name is null");
    }

    private void validate(Object field, String errorMessage) throws UserValidationException {
        if (field == null) {
            throw new UserValidationException(errorMessage);
        }
    }

}

