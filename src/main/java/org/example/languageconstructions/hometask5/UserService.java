package org.example.languageconstructions.hometask5;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    private final List<User> userList = new ArrayList<>();
    private final UserValidator userValidator;

    public UserService(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    public void saveUser(User user) throws UserValidationException {
        userValidator.validate(user);
        userList.add(user);
    }

    public User getUserById(int id) throws UserNotFoundException {
        for (User user : userList) {
            if (user.getId() == id) {
                return user;
            }
        }
       throw new UserNotFoundException("User not found");
    }
}
