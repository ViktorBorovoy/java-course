package org.example.classesandmethods;

public class PrinterTest {
    public static void main(String[] args) {
        Printer printer = new Printer();
        printer.print("Ivan", 23, "099494949");

        printer.print("Ivan", "099494949", 23);

        String result = printer.print("Ivan", "099494949", "23");

        System.out.println(result);
    }
}
