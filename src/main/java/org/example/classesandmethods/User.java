package org.example.classesandmethods;

public class User {
    protected final String name;
    private final String email;
    private final String phone;
    private final int age;

    public User(String name, String email, String phone, int age) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.age = age;
    }

    public User(String name, String email, String phone) {
        this(name, email, phone, 30);
    }

    public User(String name, String email) {
        this(name, email, "", 30);
    }

    public User(String name) {
        this(name, "", "", 30);
    }

    public User() {
        this("", "", "", 0);
    }

    public String getFullInfo() {
        return "Name: " + name + " Email: " + email + " phone: " + phone + " age: " + age;
    }
}
