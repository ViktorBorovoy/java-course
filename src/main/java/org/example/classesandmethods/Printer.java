package org.example.classesandmethods;

 public class Printer {

    public void print(String name, int age, String phone) {
        String result = name + ":" + age + ":" + phone;
        System.out.println(result);
    }

    public void print(String name, String phone, int age) {
        String result = name + ":" + age + ":" + phone + ":";
        System.out.println(result);
    }

    public String print(String name, String phone, String age) {
        String result = name + ":" + age + ":" + phone + ":";

        return result;
    }
}
