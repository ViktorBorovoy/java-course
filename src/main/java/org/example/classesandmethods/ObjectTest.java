package org.example.classesandmethods;

 public class ObjectTest {
    public static void main(String[] args) {
        Object obj = new Object();
        Object obj2 = new Object();

        System.out.println(obj.equals(obj2));

        System.out.println("Hash code:   " + obj.hashCode());
        System.out.println("String representation" + obj.toString());

        Class<?> aClass = obj.getClass();

        Car car = new Car(4, "DMW", "RED");
        Car car2 = new Car(4, "DMW", "RED");

        System.out.println(car);

        System.out.println(car.equals(car2));


    }
}
