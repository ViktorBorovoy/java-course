package org.example.classesandmethods;

 public class CarTest {
    public static void main(String[] args) {

        Car car = new Car(4, "BMW", "black");

        car.startEngine();

        String model = car.getModel();

        System.out.println(model);

        System.out.println(car);
    }

}
