package org.example.classesandmethods;

public class Calculator {

    private double x;
    private double y;
    private String value;

    public Calculator(double x, double y, String value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public void calculate () {
        double result = 0;
        if (value.equals("+"))
            result = x + y;
        else if (value.equals("-"))
            result = x - y;
        else if (value.equals("*"))
            result = x * y;
        else if (value.equals("/"))
            result = x / y;

        System.out.println(result);


    }

    /*   public int sum(int x, int y) {
        return x + y;
    }
    public int minus(int x, int y) { return  x - y;}
    public int multiply (int x, int y) { return x * y;}
    public int div(int x, int y) { return x / y; }
*/

    }
