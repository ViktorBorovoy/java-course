package org.example.classesandmethods;

public class Car {
    private int wheelsCount;
    private String model;
    private String color;

    public Car(int wheelsCount, String model, String color) {
        System.out.println("Car fields initialization...");
        this.wheelsCount = wheelsCount;
        this.model = model;
        this.color = color;
    }

    public void startEngine() {
        System.out.println("Engine is starting...");
    }

    public String getModel() {
        return this.model;
    }

   @Override
    public String toString() {
        return "Wheels count: " + wheelsCount + " model " + model;
    }
}
