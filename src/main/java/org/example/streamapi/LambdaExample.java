package org.example.streamapi;

public class LambdaExample {
    public static void main(String[] args) {
        User user = new User() {
            @Override
            public String getName() {
                return "name";
            }
        };

        User user2 = () -> "name";


        UserWithParams userWithParams = new UserWithParams() {
            @Override
            public String getId(String name) {
                return "Parameter length" + name.length();
            }
        };

        UserWithParams userWithParams2 = (param) -> "Parameter length" + param.length();
    }

    public interface User {
        String getName();
    }

    public interface UserWithParams {
        String getId(String name);
    }
}
