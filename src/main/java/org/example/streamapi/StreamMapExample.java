package org.example.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamMapExample {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        List<User> users = new ArrayList<>();


        names.add("Tom");
        names.add("John");
        names.add("Jack");

        for (String name : names) {
            User user = new User(name);
            users.add(user);
        }
        System.out.println(users);

        List<User> usersList = names.stream().map(name -> new User(name)).collect(Collectors.toList());

        List<User> list = names.stream()
                .filter(name -> name.startsWith("J"))
                .map(name -> new User(name))
                .collect(Collectors.toList());

        System.out.println(list);

        List<User> namesCollection = names.stream().map(name -> new User(name))
                .collect(Collectors.toList());
        System.out.println(namesCollection);
    }
    private static class User{
        private final String name;

        public User(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

}
