package org.example.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class FindAnyFindFirstExample {
    public static void main(String[] args) {
        List<UserName> userNames = new ArrayList<>();
        userNames.add(new UserName("Petro"));
        userNames.add(new UserName("Ivan"));
        userNames.add(new UserName("Irakli"));
        userNames.add(new UserName("Mana"));

        UserName firstUserName = getFirstUserName(userNames);
        if (firstUserName != null) {
            System.out.println(firstUserName);
        }


        boolean present = userNames.stream()
                .filter(userName -> userName.getName().startsWith("I"))
                .findFirst()
                .isPresent();
    }

    private static UserName getFirstUserName(List<UserName> userNames) {
        for (UserName userName : userNames) {
            if (userName.getName().startsWith("I")) {
                return userName;
            }
        }
        return null;
    }

    private static class UserName {
        private final String name;

        private UserName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "UserName{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
