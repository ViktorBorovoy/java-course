package org.example.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamFilterExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        List<String> newList = new ArrayList<>();

        list.add("wqewqe");
        list.add("wqewqedsadasd");
        list.add("wqewqesada");
        list.add("wqewqesdadsadsadsadsa");

        for (String s : list) {
            if (s.length() <= 10){
                newList.add(s);
            }
        }
        System.out.println(newList);


        List<String> strings = list.stream()
                .filter(s -> s.length() <= 10)
                .collect(Collectors.toList());

        System.out.println("Stream version: " + strings);

        List<String> longStrings = list.stream().
                filter(s -> s.length() > 10)
                .collect(Collectors.toList());

        System.out.println(longStrings);
    }
}
