package org.example.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableExample {
  public static void main(String[] args) {
    ExecutorService executorService = Executors.newFixedThreadPool(2);
    Future<String> stringFuture = executorService.submit(new Callable<String>() {
      @Override
      public String call() throws Exception {
        Thread.sleep(10000);
        return "Hello from thread";
      }
    });

    try {
      System.out.println("main is going to be blocked.");
      String str = stringFuture.get();
      System.out.println("main is unblocked");
      System.out.println(str);
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }
}
