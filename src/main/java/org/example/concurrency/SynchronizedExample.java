package org.example.concurrency;

public class SynchronizedExample {
  public static void main(String[] args) {

    Account account = new Account(100.0, 200.0);
    account.transferMoney(50);


    System.out.println(account);

  }
}
