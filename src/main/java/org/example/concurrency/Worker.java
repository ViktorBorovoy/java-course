package org.example.concurrency;

public class Worker extends Thread {

  @Override
  public void run() {
    System.out.println("Worker started");
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(Thread.currentThread().getName());
    System.out.println("I'm in new thread");


  }
}
