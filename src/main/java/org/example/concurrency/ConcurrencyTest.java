package org.example.concurrency;

import java.util.ArrayList;
import java.util.List;

public class ConcurrencyTest {
    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<>();
        myList.add(10);
        myList.add(5);
        myList.add(421);
        myList.add(2);
        myList.add(41);
        myList.add(512);
        myList.add(54);
        myList.add(88);
        myList.add(11);
        myList.add(666);

        CalculateSumThread myThread = new CalculateSumThread(myList);
        myThread.start();
        System.out.println(Thread.currentThread().getName());
    }
}
