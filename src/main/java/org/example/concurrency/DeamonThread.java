package org.example.concurrency;

public class DeamonThread {
  public static void main(String[] args) throws InterruptedException {
    Worker worker = new Worker();
    worker.setDaemon(true);
    worker.start();


    Thread.sleep(2000);
    System.out.println("End main");
  }
}
