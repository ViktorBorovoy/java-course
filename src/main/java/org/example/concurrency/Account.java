package org.example.concurrency;

public class Account {

  private double accountFrom;
  private double accountTo;

  public Account(double accountFrom, double accountTo) {
    this.accountFrom = accountFrom;
    this.accountTo = accountTo;
  }

  public synchronized void transferMoney(double sum) {
    if (this.accountFrom < sum) {
      throw new RuntimeException("not enough money");
    }
    this.accountFrom -= sum;
    this.accountTo += sum;
  }

  @Override
  public String toString() {
    return "Account{" +
        "accountFrom=" + accountFrom +
        ", accountTo=" + accountTo +
        '}';
  }
}
