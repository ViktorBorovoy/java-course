package org.example.concurrency;

public class AccountSyncBlockExample {

  private final Object lock1 = new Object();
  private final Object lock2 = new Object();

  private double accountFrom;
  private double accountTo;

  public AccountSyncBlockExample(double accountFrom, double accountTo) {
    this.accountFrom = accountFrom;
    this.accountTo = accountTo;
  }

  public void transferMoney(double sum) {
    synchronized (lock1) {
      if (this.accountFrom < sum) {
        throw new RuntimeException("not enough money");
      }
      this.accountFrom -= sum;
      this.accountTo += sum;
    }
  }

  public void doSmt() {
    synchronized (lock2) {
      System.out.println("do smt");
    }
  }

  @Override
  public String toString() {
    return "Account{" +
        "accountFrom=" + accountFrom +
        ", accountTo=" + accountTo +
        '}';
  }
}
