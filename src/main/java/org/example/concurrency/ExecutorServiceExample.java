package org.example.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceExample {
  public static void main(String[] args) {
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    for (int i = 0; i < 100; i++) {
      executorService.submit(new Runnable() {
        @Override
        public void run() {
          System.out.println("hello");
        }
      });
    }

    executorService.shutdown();
    executorService.shutdownNow();
  }
}
