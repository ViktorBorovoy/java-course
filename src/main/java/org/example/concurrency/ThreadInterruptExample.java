package org.example.concurrency;

import java.io.File;
import java.io.FileInputStream;

public class ThreadInterruptExample implements Runnable {

  @Override
  public void run() {
    File file = new File("test.txt");
    try {
      FileInputStream fileInputStream = new FileInputStream(file);
      int read = fileInputStream.read();

      boolean interrupted = Thread.interrupted();
      if (interrupted) {
        fileInputStream.close();

        return;
      }

      System.out.println(read);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public static void main(String[] args) {
    ThreadInterruptExample threadInterruptExample = new ThreadInterruptExample();
    Thread thread = new Thread(threadInterruptExample);
    thread.start();

    thread.interrupt();
  }
}
