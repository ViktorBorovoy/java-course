package org.example.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample {
  private AtomicInteger atomicInteger = new AtomicInteger(0);

  public void increment() {
    atomicInteger.getAndIncrement();
  }

  public static void main(String[] args) throws InterruptedException {
    AtomicExample atomicExample = new AtomicExample();
    ExecutorService executorService = Executors.newFixedThreadPool(100);
    for (int i = 0; i < 10; i++) {
      executorService.submit(new Runnable() {
        @Override
        public void run() {
          try {
            Thread.sleep(10);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          atomicExample.increment();
        }
      });
    }
    executorService.shutdown();

    Thread.sleep(1000);

    System.out.println(atomicExample.atomicInteger.get());
  }
}
