package org.example.concurrency;

public class ThreadTest {
  public static void main(String[] args) {
//    create thread version 1
    Worker workerThread = new Worker();
    workerThread.start();

    System.out.println(Thread.currentThread().getName());
    System.out.println("main thread");

//    create thread version 2
    WorkerRunnable workerRunnable = new WorkerRunnable();

    //    NEW state
    Thread workerRunnableThread = new Thread(workerRunnable);

//    get thread state
    Thread.State state = workerRunnableThread.getState();

//    RUNNABLE state
    workerRunnableThread.start();

    System.out.println("End main");
  }
}
