package org.example.concurrency;

import java.util.List;

public class CalculateSumThread extends Thread{
    private final List<Integer> myList;

    public CalculateSumThread(List<Integer> myList) {
        this.myList = myList;
    }

    @Override
    public void run() {
        Integer result = 0;
        for (Integer integer : this.myList) {
            result += integer;
        }
        System.out.println(result);
        System.out.println(Thread.currentThread().getName());
    }
}
