package org.example.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VolatileExamle {

  private volatile int count = 0;

  public void increment() {
    count++;
  }

  public static void main(String[] args) throws InterruptedException {
    VolatileExamle volatileExamle = new VolatileExamle();
    ExecutorService executorService = Executors.newFixedThreadPool(100);

    for (int i = 0; i < 10; i++) {
      executorService.submit(new Runnable() {
        @Override
        public void run() {
          try {
            Thread.sleep(10);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          volatileExamle.increment();
        }
      });
    }
    executorService.shutdown();

    Thread.sleep(1000);

    System.out.println(volatileExamle.count);
  }
}
