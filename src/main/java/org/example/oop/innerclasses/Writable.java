package org.example.oop.innerclasses;

public interface Writable {
    void write();
}
