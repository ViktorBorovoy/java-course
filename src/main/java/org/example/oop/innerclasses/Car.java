package org.example.oop.innerclasses;

public class Car {
    private static final int wheelsCount = 4;

    private String model;

    public Car(String model) {
        this.model = model;
    }

    public void startEngine() {
        Engine engine = new Engine("VTec");
        engine.start();
    }

    public static class Engine {
        private static final double power = 150.0;

        private String modification;

        public Engine(String modification) {
            this.modification = modification;
        }

        public String getModification() {
            return modification;
        }

        public void setModification(String modification) {
            this.modification = modification;
        }

        public void start() {
            System.out.println("Wheels count" + wheelsCount);
            System.out.println("start");
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
