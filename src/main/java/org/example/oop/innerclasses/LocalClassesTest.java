package org.example.oop.innerclasses;

public class LocalClassesTest {

    public static int x = 7;
    public int y = 12;

    public static void main(String[] args) {

    }

    public void runDaily(String day) {
        String workerName = "W1";

        class Worker {
            void work() {
                System.out.println(y);
                System.out.println(x);
                System.out.println(workerName);
                System.out.println(day);
                System.out.println("do work");
            }
        }

        Worker worker = new Worker();
        worker.work();
    }
}
