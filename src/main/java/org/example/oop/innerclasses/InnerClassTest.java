package org.example.oop.innerclasses;

public class InnerClassTest {
    public static void main(String[] args) {
        Flat flat = new Flat(12.2);

        Flat.Bathroom bathroom = new Flat(30).new Bathroom(12.2);
    }
}
