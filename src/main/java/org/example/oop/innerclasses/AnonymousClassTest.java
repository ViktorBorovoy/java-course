package org.example.oop.innerclasses;

public class AnonymousClassTest {
    public static void main(String[] args) {
        new Writable() {
            @Override
            public void write() {
                System.out.println("hello world!");
            }
        };

    }
}
