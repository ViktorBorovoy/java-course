package org.example.oop.innerclasses;

public class Flat {
    private static final int cornersCount = 4;

    private double square;

    public Flat(double square) {
        this.square = square;

        Bathroom bathroom = new Bathroom(12);
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public class Bathroom {
        private static final int bathroomCornersCount = 6;

        private double bathRoomSquare;

        public Bathroom(double square) {
            this.bathRoomSquare = square;
        }

        public double getBathRoomSquare() {
            return bathRoomSquare;
        }

        public void setBathRoomSquare(double bathRoomSquare) {
            this.bathRoomSquare = bathRoomSquare;
        }
    }
}
