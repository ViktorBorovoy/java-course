package org.example.oop.principles.polimofrism;

public class Printer {

    public void print(Writable writable) {
        System.out.println("I am printer");
        System.out.println("I am printing");

        writable.write();
    }
}
