package org.example.oop.principles.polimofrism;

public class Pencil implements Writable {
    @Override
    public void write() {
        System.out.println("////////");
        System.out.println("////////");
        System.out.println("//pencil///");
        System.out.println("////////");
        System.out.println("////////");
    }
}
