package org.example.oop.principles.polimofrism.hometask;

public class SingerTest {
    public static void main(String[] args) {

        Singer popSinger = new PopSinger("Катя", 25);
        Singer rockSinger = new RockSinger("Витя", 27);

        popSinger.sing();
        rockSinger.sing();
        callSinger(popSinger);
        callSinger(rockSinger);
        callSinger(new PopSinger("Eugene", 7));

    }
    public static void callSinger(Singer singer) {
        singer.sing();
    }
}
