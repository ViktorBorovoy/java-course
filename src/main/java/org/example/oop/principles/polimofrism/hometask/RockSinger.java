package org.example.oop.principles.polimofrism.hometask;

public class RockSinger implements Singer {
    private String name;
    private int age;

    public RockSinger(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void sing() {
        System.out.println(toString()+ " я люблю играть рок музыку");
    }

    @Override
    public String toString() {
        return "Я rockSinger" + name + ", мне " + age + " лет";
        }
    }

