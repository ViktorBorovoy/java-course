package org.example.oop.principles.polimofrism;

public interface Writable {
    void write();
}
