package org.example.oop.principles.polimofrism.hometask;

public class PopSinger implements Singer {
    private String name;
    private int age;

    public PopSinger(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void sing() {
        System.out.println(toString()+ " я люблю петь");
    }

    @Override
    public String toString() {
        return "Я PopSinger " + name + ", мне " + age + " лет";
    }
}
