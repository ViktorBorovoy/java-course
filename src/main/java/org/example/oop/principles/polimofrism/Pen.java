package org.example.oop.principles.polimofrism;

public class Pen implements Writable {
    @Override
    public void write() {
        System.out.println("=========");
        System.out.println("=========");
        System.out.println("=====pen====");
        System.out.println("=========");
        System.out.println("=========");
    }
}
