package org.example.oop.principles.polimofrism;

public class PolimorfismTest {
    public static void main(String[] args) {
        Printer printer = new Printer();

        Writable pen = new Pen();
        Writable pencil = new Pencil();

        String today = "Saturday";

        if (today.equals("Saturday")) {
            printer.print(pen);
        } else {
            printer.print(pencil);
        }
    }
}
