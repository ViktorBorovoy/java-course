package org.example.oop.principles.polimofrism.hometask;

public interface Singer {
    void sing();
}
