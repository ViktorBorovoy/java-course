package org.example.oop.principles.encapsulation.hometask;

public class Owner {
    private String name;
    private int age;

    public String getName() {
        return "Hello " + name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return "Happy birthday with your " + age + " years";
    }

    public void setAge(int age) {
        this.age = age;
    }
}
