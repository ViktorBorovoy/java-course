package org.example.oop.principles.encapsulation.hometask;

public class Phone {

    private String brand;

    public String getBrand() { return brand + "ver2.0" ; }

    public void start() {
        String phoneName = getPhoneName();
        enableStarting(phoneName);
        System.out.println("starting " + phoneName);
    }

    private void enableStarting(String phoneName) {

    }

    private String getPhoneName() {

        System.out.println("Samsung");
        return "Samsung";
    }
}
