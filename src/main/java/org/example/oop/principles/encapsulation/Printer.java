package org.example.oop.principles.encapsulation;

public class Printer {

    private String model;

    public String getModel() {
        return model + " V1";
    }

    public void print() {
        String driverName = getDriverName();

       controlPrinting(driverName);

        System.out.println("printing " + driverName);

    }

    private void controlPrinting(String driverName) {

    }
    private String getDriverName() {
        System.out.println("retrieving driver and control print process");

        return "driver1";
    }
}
