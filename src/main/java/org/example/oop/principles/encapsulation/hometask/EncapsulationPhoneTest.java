package org.example.oop.principles.encapsulation.hometask;

public class EncapsulationPhoneTest {
    public static void main(String[] args) {
        Phone phone = new Phone();
        phone.start();

        Owner owner = new Owner();
        owner.setAge(27);
        owner.setName("Viktor");

        System.out.println(owner.getName());
        System.out.println(owner.getAge());
    }

}
