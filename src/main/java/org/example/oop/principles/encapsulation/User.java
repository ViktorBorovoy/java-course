package org.example.oop.principles.encapsulation;

public class User {
    private String name;
    private int age;

    public String getName() {
        return "Mr " + name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
