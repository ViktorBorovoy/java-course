package org.example.oop.principles.encapsulation;

public class EncapsulationTest {
    public static void main(String[] args) {
        Printer printer = new Printer();
        printer.print();

        User user = new User();
        user.setAge(23);
        user.setName("user1");

        System.out.println(user.getName());
    }
}
