package org.example.oop.principles.inheritance;

public class Tiger extends Animal {

    private String color;

    public Tiger(String breed, int legsCount, String color) {
        super(breed, legsCount);

        this.color = color;
    }

    @Override
    public int getLegsCount() {
        System.out.println("child legs count method");
        return 4;
    }
}
