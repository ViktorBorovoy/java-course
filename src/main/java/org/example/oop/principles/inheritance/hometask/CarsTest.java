package org.example.oop.principles.inheritance.hometask;

public class CarsTest {
    public static void main(String[] args) {
        Car car = new Car("Renault", "white", 4);
        Renault renault = new Renault("Renault", "white", 4, "logan");

        System.out.println(car.getColor());
        System.out.println(renault.getColor());

        System.out.println(car);
        System.out.println(renault);
    }
}
