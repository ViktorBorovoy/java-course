package org.example.oop.principles.inheritance.hometask;

public class Car {
    String brand;
    String color;
    int doorsCount;

    public Car (String brand, String color, int doorsCount) {
        this.brand = brand;
        this.color = color;
        this.doorsCount = doorsCount;
    }

    public String getBrand() { return brand; }

    public void setBrand(String brand) { this.brand = brand; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public int getDoorsCount() { return doorsCount; }

    public void setDoorsCount(int doorsCount) { this.doorsCount = doorsCount; }

    @Override
    public String toString() {
        return "Cars{" +
                "brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", doorsCount=" + doorsCount +
                '}';
    }
}
