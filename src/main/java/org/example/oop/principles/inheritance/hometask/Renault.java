package org.example.oop.principles.inheritance.hometask;

public class Renault extends Car {

    private String model;

    public Renault(String brand, String color, int doorsCount, String model) {
        super(brand, color, doorsCount);

        this.model = model;
    }

    @Override
    public String toString() {
        return super.toString() + model;
    }
}
