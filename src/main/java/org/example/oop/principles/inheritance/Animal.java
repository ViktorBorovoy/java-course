package org.example.oop.principles.inheritance;

public class Animal {
    private String breed;
    private int legsCount;

    public Animal(String breed, int legsCount) {
        this.breed = breed;
        this.legsCount = legsCount;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getLegsCount() {
        System.out.println("parent legs count method");
        return legsCount;
    }

    public void setLegsCount(int legsCount) {
        this.legsCount = legsCount;
    }
}
