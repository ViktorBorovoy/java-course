package org.example.oop.principles.inheritance;

public class InheritanceTest {
    public static void main(String[] args) {
        Animal animal = new Animal("animal", 4);
        Tiger tiger = new Tiger("breed", 4, "orange");

        System.out.println(animal.getLegsCount());
        System.out.println(tiger.getLegsCount());
    }
}
