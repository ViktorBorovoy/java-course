package org.example.oop.initializationorder;

public class Parent {

    private String x = getNonStaticValue();

    static {
        System.out.println("static block");
    }

    public static String stStr = getStaticValue();

    {
        System.out.println("non static block");
    }

    public String getNonStaticValue() {
        System.out.println("non static var");
        return "";
    }

    public static String getStaticValue() {
        System.out.println("static var");
        return "";
    }

    public Parent() {
        System.out.println("constructor");
    }
}
