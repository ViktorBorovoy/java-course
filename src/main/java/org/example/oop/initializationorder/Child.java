package org.example.oop.initializationorder;

public class Child extends Parent {

    private String x = getChildNonStaticValue();

    static {
        System.out.println("child static block");
    }

    public static String stStr = getStaticValue();

    {
        System.out.println("child non static block");
    }

    public String getChildNonStaticValue() {
        System.out.println("child non static var");
        return "";
    }

    public static String getStaticValue() {
        System.out.println("child static var");
        return "";
    }

    public Child() {
        System.out.println("child constructor");
    }
}
