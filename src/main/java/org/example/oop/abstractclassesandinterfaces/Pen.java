package org.example.oop.abstractclassesandinterfaces;

public class Pen implements Writable {

    @Override
    public void write() {
        System.out.println("pen write");
    }
}
