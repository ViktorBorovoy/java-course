package org.example.oop.abstractclassesandinterfaces;

public class VolvoCar extends AbstractCar {

    public VolvoCar(int year) {
        super("Volvo", year);
    }

    @Override
    public void startEngine() {
        System.out.println("start volvo");
    }
}
