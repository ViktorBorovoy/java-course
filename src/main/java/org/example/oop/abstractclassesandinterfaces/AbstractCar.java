package org.example.oop.abstractclassesandinterfaces;

public abstract class AbstractCar {
    private String model;
    private int year;

    public AbstractCar(String model, int year) {
        this.model = model;
        this.year = year;
    }

    public abstract void startEngine();
}
