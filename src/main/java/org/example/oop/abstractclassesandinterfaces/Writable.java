package org.example.oop.abstractclassesandinterfaces;

public interface Writable {

    int x = 10;

    void write();
}
