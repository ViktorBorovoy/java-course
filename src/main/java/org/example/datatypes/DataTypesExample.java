package org.example.datatypes;

public class DataTypesExample {
    public static void main(String[] args) {
//        примитивные типы данных
        int x = 123;
        long l = 123;
        float y = 12.4f;
        double d = 123.4;
        char ch = 'a';
        boolean b = true;
        byte bb = 124;

//        ссылочные типы данных
        String s = "hello";

        s = null;

        s = "hello";

        Integer in = 20;
        Long lb = 20L;



    }
}
