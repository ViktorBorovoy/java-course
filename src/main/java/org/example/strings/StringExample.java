package org.example.strings;

public class StringExample {
    public static void main(String[] args) {
        String str = "hell";
        String str2 = "hello";

//        compare two strings

        if (str.equals(str2)) {
            System.out.println("equal");
        } else {
            System.out.println("not equal");
        }

//        contains

        System.out.println(str2.contains("llo"));

        String address = "Kharkiv Lenina 35";

        System.out.println(address.substring(3, 6));
        System.out.println(address.substring(0, 7));

//        split

        String[] addressParts = address.split(" ");

        System.out.println("Street: " + addressParts[1]);

//        length of string

        System.out.println("Length");
        System.out.println(address.length());

        System.out.println("Start with ends with");
        System.out.println(address.endsWith("hello"));
        System.out.println(address.startsWith("K"));

        System.out.println("Replace");
        String replace = address.replace(" ", ",");
        System.out.println(replace);

        System.out.println("Index of");

        System.out.println(address.indexOf("a"));

//        remove spaces in the beginning and end
        String s = " rrrr ";
        System.out.println("Length with spaces: " + s.length());
        String trim = s.trim();
        System.out.println("Length without spaces: " + trim.length());


        System.out.println("To upper case");
        String hello = "hello";

        System.out.println(hello.toUpperCase());

        String hello2 = "HELLO";

        System.out.println(hello2.toLowerCase());

    }
}
